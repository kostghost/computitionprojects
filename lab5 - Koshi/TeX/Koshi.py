# -*- coding: utf-8 -*-

import math
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.patches import Rectangle
import pylab

MIN_X = 1
MAX_X = 2
MIN_Y = -0.9
MAX_Y = 0
H = 0.3
EXACT_ANSWER_COLOR = "b"
APPROX_ANSWER_COLOR = "r"

N = math.floor((MAX_X - MIN_X) / H)

# Utilities
def pairs_to_list(pairs_xy):
    xs, ys = [], []
    for (x, y) in pairs_xy:
        xs.append(x)
        ys.append(y)
    return xs, ys

def draw_axes():
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.axis([MIN_X, MAX_X, MIN_Y, MAX_Y])
    plt.plot([MIN_X, MAX_X], [0, 0], "k-")

def draw_result(pairs_xy, color):
    xs, ys = pairs_to_list(pairs_xy)
    plt.plot(xs, ys, color + "-")

#генерирует сетку по X
def generate_grid(x0, x1, n):
    assert (x0 < x1 and n >= 1)
    h = (x1 - x0) / n
    xs = [x0]
    for i in range(n):
        xs.append(x0 + (i + 1) * h)
    return xs

#рисует прямоугольники цветом графика
def draw_legend_rect(x, y, color):
    current_axis = plt.gca()
    rect = mpatches.Rectangle(height=0.05, width=0.08, xy=[x, y], alpha=1, color=color)
    current_axis.add_patch(rect)

def save_to_file(pairs_xy):
	f = open('text.txt', 'w')
	for (x, y) in pairs_xy:
		f.write('{0:.4}'.format(str(x)) + '\t'  + str(y) + '\n')
# endof Utilities

# exact
def exact_answer(x):
    return 1 / (x ** 2) - 1

# tailor
# der1 = y' = f(x,y)
def der1(xi, yi):
    return -2 * (yi+1) / xi
def der2(xi, yi):
    return 6 * (yi+1) / (xi**2)
def der3(xi, yi):
    return -24*(yi+1) / (xi**3)
def tailor3_func(xi, yi, xi1):
    return yi + der1(xi, yi)*(xi1-xi) + der2(xi, yi)*((xi1-xi)**2) / 2 + der3(xi, yi)*((xi1-xi)**3) / 6

def tailor3(y0, grid):
    grid_size = len(grid)
    assert (grid_size >= 2)
    h = grid[1] - grid[0]
    result = [(grid[0], y0)]
    for i in range(grid_size-1):
        (xi, yi) = result[i]
        yi1 = tailor3_func(xi, yi, grid[i+1])
        result.append((grid[i+1], yi1))
    return result
# endof tailor

#euler
def explicit_euler(y0, grid):
    grid_size = len(grid)
    assert (grid_size >= 2)
    h = grid[1] - grid[0]
    result = [(grid[0], y0)]
    for i in range(grid_size - 1):
        (xi, yi) = result[i]
        yi1 = yi - h * 2 * (yi + 1) / xi
        result.append((grid[i + 1], yi1))
    return result
# endof euler

#Simpson
def simpson_yi(xi, preResult, prepreResult, h):
	(xPre, yPre) =  preResult
	(xPrePre, yPrePre) = prepreResult
	return (3*xi*(yPrePre + h/3*(4*der1(xPre, yPre) + der1(xPrePre,yPrePre))) - 2*h) / (3*xi + 2*h)

def simpson(y0, grid):
	grid_size = len(grid)
	assert (grid_size >= 2)
	h = grid[1] - grid[0]
	result = [(grid[0], y0)]
	#используем метод Эйлера для получения y1
	(x0, y0) = result[0]
	result.append((grid[1], y0 - h * 2 * (y0 + 1) / x0))
	#для остального методом Симпсона
	for i in range(grid_size - 2):
		yi = simpson_yi(grid[i + 2], result[i+1], result[i], h)
		result.append((grid[i + 2], yi))

	return result

#endof Simpson


# Tests
def test_explicit_euler(x0, x1, n):
    grid = generate_grid(x0, x1, n)
    points = explicit_euler(0, grid)
    save_to_file(points)
    draw_axes()
    draw_result(points, APPROX_ANSWER_COLOR)
    draw_exact_answer(EXACT_ANSWER_COLOR)
    draw_legend_rect(1.55, -0.2, "blue")
    draw_legend_rect(1.55, -0.28, "red")
    pylab.text(1.65, -0.18, u'Точное решение', fontdict={'family': 'verdana'})
    pylab.text(1.65, -0.26, u'Явный метод Эйлера', fontdict={'family': 'verdana'})
    plt.title("h = " + str(H))
    plt.show()

def test_tailor3(x0, x1, n):
    grid = generate_grid(x0, x1, n)
    points = tailor3(0, grid)
    save_to_file(points)
    draw_axes()
    draw_result(points, APPROX_ANSWER_COLOR)
    draw_exact_answer(EXACT_ANSWER_COLOR)
    draw_legend_rect(1.45, -0.2, "blue")
    draw_legend_rect(1.45, -0.28, "red")
    pylab.text(1.55, -0.18, u'Точное решение', fontdict={'family': 'verdana'})
    pylab.text(1.55, -0.26, u'Метод Тейлора 3 порядка', fontdict={'family': 'verdana'})
    plt.title("h = " + str(H))
    plt.show()

def test_simpson(x0, x1, n):
    grid = generate_grid(x0, x1, n)
    points = simpson(0, grid)
    save_to_file(points)
    draw_axes()
    draw_result(points, APPROX_ANSWER_COLOR)
    draw_exact_answer(EXACT_ANSWER_COLOR)
    draw_legend_rect(1.55, -0.2, "blue")
    draw_legend_rect(1.55, -0.28, "red")
    pylab.text(1.65, -0.18, u'Точное решение', fontdict={'family': 'verdana'})
    pylab.text(1.65, -0.26, u'Метод Симпсона', fontdict={'family': 'verdana'})
    plt.title("h = " + str(H))
    plt.show()


def draw_exact_answer(color):
    xs = generate_grid(MIN_X, MAX_X, 500)
    ys = [exact_answer(x) for x in xs]
    plt.plot(xs, ys, color + "-")
# endof Tests


#test_explicit_euler(MIN_X, MAX_X, N)
test_tailor3(MIN_X, MAX_X, N)
#test_simpson(MIN_X, MAX_X, N)

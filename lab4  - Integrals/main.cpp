#include <iostream>
double LeftRectangle(double (*func)(double x), double A, double B, double h )  {
    double result = 0;

    int n = (int)floor((B - A) / h);
    for (int i = 0; i <= n - 1; i++) {
        result += func(A + i*h);
    }
    return result * h;
}

double Euler(double(*func)(double x), double(*funcDer)(double x), double A, double B, double h) {
    double result = 0;
    int n = (int)floor((B - A) / h);    

    result += (h / 2) * (func(A) + func(B));
    result += (pow(h, 2) / 12) * (funcDer(A) - funcDer(B));

    for (int i = 1; i <= n - 1; i++) {
        result += h * func(A + i*h);
    }
    return result;
}

double func(double x) {
    return log(1 + pow(x, 2));
}

double funcDerivative(double x) {
    return (2 * x) / (1 + pow(x, 2));
}

int main() {
    using namespace std;

    const double A = 0;
    const double B = 1;
    const double STEPS[] = { 0.1 , 0.05, 0.025 };

    cout << "Left Rectangles method: " << endl;
    for each (double step in STEPS) {
        cout << "h = " << step << " \t" << LeftRectangle(func, A, B, step) << endl;
    }

    cout << endl << "Euler's method: " << endl;
    for each (double step in STEPS) {
        cout << "h = " << step << " \t" << Euler(func, funcDerivative, A, B, step) << endl;
    }

    system("pause");
}
/*
������� 5

���������� ����� ��������� f(x) = sin(x) + 0.1 - 1.4*x^2 
������� �������, ���������������� ������� ������� � 
������� ������� ��������

������� ����������, 2016�
kostghost@gmail.com
*/

#include <iostream>
#include <math.h>

//��������� ��������� �� ���� a ����� � �������-����������� ����� b ������� epsilon
inline bool IsEqualInEpsilon(double a, double b, double epsilon) {
	return fabs(a - b) < epsilon;
}

//���� �������
inline double Func(double x) {
	return sin(x) + 0.1 - 1.4*(pow(x, 2));
}
//������ ����������� Func
inline double FuncFirstDerivative(double x) {
	return cos(x) - 2.8 * x;
}

//������� ��
inline double Phi(double x) {
	return sqrt((sin(x) + 0.1) / 1.4);
}

//����� �������
double FindingRootsNewton(double (*F)(double x), double (*FDer)(double x), double x0, double epsilon, int *steps){
	double prevX = x0;
	double nextX = x0;
	*steps = 0;
	
	do {
		(*steps)++;
		prevX = nextX;
		nextX = prevX - (F(prevX) / (FDer(prevX)));
	} while (!IsEqualInEpsilon(prevX, nextX, epsilon));

	return nextX;
}

//���������������� ����� �������
double FindingRootsNewtonModified(double(*F)(double x), double(*FDer)(double x), double x0, double epsilon, int *steps) {
	double prevX = x0;
	double nextX = x0;
	double funcFirstDerInX0 = FDer(x0);
	*steps = 0;

	do {
		(*steps)++;
		prevX = nextX;
		nextX = prevX - (F(prevX) / funcFirstDerInX0);
	} while (!IsEqualInEpsilon(prevX, nextX, epsilon));

	return nextX;
}

//����� ������� ��������
double FindingRootsFPI(double (*Phi)(double x), double x0, double epsilon, int *steps) {
	double prevX = x0;
	double nextX = x0;
	*steps = 0;

	do {
		(*steps)++;
		prevX = nextX;
		nextX = Phi(prevX);
	} while (!IsEqualInEpsilon(prevX, nextX, epsilon));

	return nextX;
}

int main() {
	using namespace std;

	std::cout.setf(std::ios::fixed);
	std::cout.precision(5); //������ ���������� ������ ����� ������� ��� ������

	double epsilon = 0.00005;
	int stepsNewton, stepsNewtonModified, stepsIter; //������ ����� ����� ��� �������

	double rootNewton = FindingRootsNewton(&Func, &FuncFirstDerivative, 1, epsilon, &stepsNewton);
	double rootNewtonModified = FindingRootsNewtonModified(&Func, &FuncFirstDerivative, 1, epsilon, &stepsNewtonModified);
	double rootIter = FindingRootsFPI(&Phi, 1, epsilon, &stepsIter);

	cout << "Epsilon = " << epsilon << endl;
	cout << "Newton's:\t root = " << rootNewton << " with " << stepsNewton << " iterations" << endl;
	cout << "Mod. Newton's:\t root = " << rootNewtonModified << " with " << stepsNewtonModified << " iterations" << endl;
	cout << "Iteration:\t root = " << rootIter << " with " << stepsIter << " iterations" << endl;
	
	system("pause");
	return 0;
}
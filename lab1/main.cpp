#define _USE_MATH_DEFINES
#include <iostream>
#include <math.h>

const double R = 0.15;

// (i + 1)/(i^3 + i*R)
inline double SequenceS(int i) {
	return (i + 1) / (pow(i, 3) + i * R);
}

// (i - r)/( i^4 + i^2*R )
inline double SequenceP(int i) {
	return (i - R) / (pow(i, 4) + pow(i, 2) * R); 
}

// (i - R*i - 2*R)/(i^4 + i^2 * R)(i + 1)  
inline double SequrenceQ(int i) {
	return (i - R*i - 2 * R) / (pow(i, 5) + pow(i, 4) + pow(i, 3)*R + pow(i, 2)*R); 
}


double CalculateSum(double (*sequenceSummand)(int currentI) ,unsigned int from, unsigned int to){

	double finalSum = 0;
	for (unsigned int i = from; i < to; i++) {
		finalSum += sequenceSummand(i);
	}

	return finalSum;
}

int main() {
	const unsigned int N = 20000001;
	const unsigned int M = 2237;
	const unsigned int L = 136;
	const double B1 = (M_PI * M_PI) / 6; 
	const double B2 = (M_PI * M_PI) / 6 - 1;

	std::cout.setf(std::ios::fixed);
	std::cout.precision(7); //num of symbols after dot

	double sN = CalculateSum(&SequenceS, 1, N);
	double pM = CalculateSum(&SequenceP, 1, M);
	double qL = CalculateSum(&SequrenceQ, 1, L);

	std::cout << "Sn = " << sN << std::endl;
	std::cout << "Pm = " << pM << std::endl;
	std::cout << "Ql = " << qL << std::endl << std::endl;

	std::cout << "Sum S: " << std::endl;
	std::cout << "original: \t" << sN << "\t i = 1 to " << N << std::endl;
	std::cout << "after 1st acc:\t" << B1 + pM	<< "\t i = 1 to " << M << std::endl;
	std::cout << "after 2nd acc:\t" << B1 + B2 + qL	<< "\t i = 1 to " << L << std::endl;

	system("pause");
	return 0;
}
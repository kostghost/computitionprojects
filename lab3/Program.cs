﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class Program
    {
        static Matrix matrixA = new Matrix(new double[3, 3] {   { 1.5,     -0.2,        0.1 },
                                                                { -0.1,     0.9996,    -0.1 },
                                                                { -0.2996,  0.2,        -0.496 } } );

        static Matrix vectorB = new Matrix(new double[3, 1] {   { 1.365     }, 
                                                                { 1.98204   },
                                                                { -1.38799  } } );

        static void Main(string[] args)
        {
            const double epsilon = 5e-20;
            Console.WriteLine("Epsilon: " + epsilon);
            Console.WriteLine("Matrix A:");
            matrixA.Print();
            Console.WriteLine("Vector b:");
            vectorB.Print();

            //начальное приближение
            var initialApprox = new double[3] { 0, 0, 0 };

            int iterations = 0;

            Matrix vecXJacoby = MpiMethods.Jacoby(matrixA, vectorB, epsilon, out iterations, initialApprox);
            Console.WriteLine(String.Format("Jacoby method. Iterations: {0}", iterations));
            vecXJacoby.Print();
            

            Matrix vecXGauss = MpiMethods.GaussZeidel(matrixA, vectorB, epsilon, out iterations, initialApprox);
            Console.WriteLine(String.Format("Gauss-Zeidel method. Iterations: {0}", iterations));
            vecXGauss.Print();
        
          }
    }
}

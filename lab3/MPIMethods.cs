﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class MpiMethods
    {
        //Найти норму разности двух векторов
        private static double GetNormOne(double[] vec1, double[] vec2)
        {
            double norm = Math.Abs(vec1[0] - vec2[0]);
            for (int h = 0; h < vec1.GetLength(0); h++)
            {
                if (Math.Abs(vec1[h] - vec2[h]) > norm)
                    norm = Math.Abs(vec1[h] - vec2[h]);
            }

            return norm;
        }

        //Нахождение x в уравнении A*x = B методом Якоби, где B - Вектор свободных членов
        public static Matrix Jacoby(Matrix matrixA, Matrix vectorB, double epsilon, out int iterations, double[] initialApproxVector = null)
        {
            int numOfVariables = matrixA.GetColsCount(); //Количетсво компонент вектора
            double[] x; //Вектор решения, n-ый шаг
            var preX = new double[numOfVariables]; //n-1 Шаг риближения
            iterations = 0;

            //задаем вектор решения через начальное приближениие. (по умолчанию нулями)
            if (initialApproxVector == null)
                x = new double[numOfVariables];
            else
                x = initialApproxVector;

            do //продолжаем, пока норма разности векторов не станет меньше epsilon
            {
                iterations++;
                for(int i = 0; i < numOfVariables; i++)
                {
                    x[i] = preX[i];
                }

                //приближаем результат для каждого элемента
                for (int i = 0; i < numOfVariables; i++)
                {
                    preX[i] = vectorB.GetElement(i);
                    for (int g = 0; g < numOfVariables; g++)
                    {
                        if (i != g)
                            preX[i] -= matrixA.GetElement(i, g) * x[g];
                    }
                    preX[i] /= matrixA.GetElement(i, i);
                }
            } while (GetNormOne(preX, x) > epsilon);

            return new Matrix(x);
        }
        
        //Нахождение x в уравнении A*x = B методом Гаусса-Зейделя, где B - Вектор свободных членов
        public static Matrix GaussZeidel(Matrix matrixA, Matrix vectorB, double epsilon, out int iterations, double[] initialApproxVector = null)
        {
            int numOfVariables = matrixA.GetColsCount(); //Количетсво компонент вектора
            var x = new double[numOfVariables];          //Вектор решения, n-ый шаг
            var preX = new double[numOfVariables];       //n-1 Шаг риближения
            iterations = 0;
            //задаем вектор решения через начальное приближениие. (по умолчанию нулями)
            if (initialApproxVector == null)
                x = new double[numOfVariables];
            else
                x = initialApproxVector;

            do
            {
                iterations++;
                for (int i = 0; i < numOfVariables; i++)
                    preX[i] = x[i];

                for (int i = 0; i < numOfVariables; i++)
                {
                    double temp = 0;
                    for (int j = 0; j < i; j++)
                        temp += (matrixA.GetElement(i,j) * x[j]);
                    for (int j = i + 1; j < numOfVariables; j++)
                        temp += (matrixA.GetElement(i, j) * preX[j]);
                    x[i] = (vectorB.GetElement(i) - temp) / matrixA.GetElement(i,i);
                }
            } while (GetNormOne(preX, x) > epsilon);

            return new Matrix(x);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    //Данный класс немного избыточен для решения данной лабораторной работы, однако 
    //данные методы помогли мне в отладке, и я не стал их удалять, 
    //тем более, они могут быть применены в похожих задачах
    class Matrix
    {
        private double[,] _matrix;

        public Matrix(int rows, int cols)
        {
            _matrix = new double[rows, cols];
        }

        public Matrix(double[,] matrix)
        {
            _matrix = matrix; 
        }

        public Matrix(double[] vector)
        {
            _matrix = new double[vector.GetLength(0), 1];
            for (int row = 0; row < vector.GetLength(0); row++)
            {
                
                _matrix[row, 0] = vector[row];
            }
        }

        public int GetRowsCount()
        {
            return _matrix.GetLength(0);
        }

        public int GetColsCount()
        {
            return _matrix.GetLength(1);
        }

        public Matrix Transpose()
        {
            var res = new double[GetColsCount(), GetRowsCount()];
            for (int row = 0; row < GetRowsCount(); row++)
            {
                for (int col = 0; col < GetColsCount(); col++)
                {
                    res[col, row] = GetElement(row, col);
                }
            }
            return new Matrix(res);
        }

        public double GetElement(int row, int col)
        {
            return _matrix[row, col];
        }

        //for vectors
        public double GetElement(int row)
        {
            return _matrix[row, 0];
        }

        public void Print()
        {
            var strBuilder = new StringBuilder();
            for (int row = 0; row < GetRowsCount();  row++ )
            {
                for (int col = 0; col < GetColsCount(); col++)
                {
                    strBuilder.AppendFormat("{0}\t", this.GetElement(row, col));
                }
                strBuilder.Append('\n');      
            }
           Console.WriteLine( strBuilder.ToString() );
        }

        public static Matrix operator *(Matrix matA, Matrix matB)
        {
            if (matA.GetColsCount() != matB.GetRowsCount()) throw new Exception("Матрицы нельзя перемножить");
            double[,] r = new double[matA.GetRowsCount(), matB.GetColsCount()];
            for (int i = 0; i < matA.GetRowsCount(); i++)
            {
                for (int j = 0; j < matB.GetColsCount(); j++)
                {
                    for (int k = 0; k < matB.GetRowsCount(); k++)
                    {
                        r[i, j] += matA.GetElement(i, k) * matB.GetElement(k, j);
                    }
                }
            }
            return new Matrix(r);
        }

        public static Matrix operator +(Matrix matA, Matrix matB)
        {
            if (matA.GetColsCount() != matB.GetColsCount() || matA.GetRowsCount() != matB.GetRowsCount())
                throw new Exception("Разный порядок матриц! Невозможно сложить");
            var res = new double[matA.GetRowsCount(), matA.GetColsCount()];

            for (int row = 0; row < matA.GetRowsCount(); row++)
            {
                for (int col = 0; col < matA.GetColsCount(); col++)
                {
                    res[row, col] = matA.GetElement(row, col) + matB.GetElement(row, col);
                }
            }

            return new Matrix(res);
        }

    }


}
